#!/bin/bash

chain_exists()
{
    [ $# -lt 1 -o $# -gt 2 ] && { 
        echo "Usage: chain_exists <chain_name> [table]" >&2
        return 1
    }
    local chain_name="$1" ; shift
    [ $# -eq 1 ] && local table="--table $1"
    iptables $table -n --list "$chain_name" >/dev/null 2>&1
}

for login in $(awk -F: '$3>=1000 {print $1}' /etc/passwd | grep -v nobody);
do
        EXISTS=true
        chain_exists out_user_$login || EXISTS=false && echo "Verificando $login"
        $EXISTS || echo "Adicionando $login ($(id -u $login))"
        $EXISTS || iptables -N out_user_$login
        $EXISTS || iptables -A OUTPUT -m owner --uid-owner $(id -u $login) -j out_user_$login
        $EXISTS || exit
        echo "Ja existe"
done
