#!/bin/bash

nome=$1
pass=$2
u_temp=$3
limit=$4

if [ ! -d /etc/SSHPlus/userteste ]; then
	mkdir /etc/SSHPlus/userteste
fi

useradd -M -s /bin/false $nome > /dev/null 2>&1
(echo $pass;echo $pass) |passwd $nome > /dev/null 2>&1

echo "$pass" > /etc/SSHPlus/senha/$nome

echo "$nome $limit" >> /root/usuarios.db

echo "#!/bin/bash
pkill -f "$nome"
userdel --force $nome
grep -v ^$nome[[:space:]] /root/usuarios.db > /tmp/ph ; cat /tmp/ph > /root/usuarios.db
rm /etc/SSHPlus/senha/$nome > /dev/null 2>&1
rm -rf /etc/SSHPlus/userteste/$nome.sh
exit" > /etc/SSHPlus/userteste/$nome.sh

chmod +x /etc/SSHPlus/userteste/$nome.sh
at -f /etc/SSHPlus/userteste/$nome.sh now + $u_temp min > /dev/null 2>&1
echo "1"