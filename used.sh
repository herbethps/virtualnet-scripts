#!/bin/bash

LIST= iptables -L OUTPUT -n -v | grep out_ | awk '$2 != "0" {print $3 " - " $2}' | awk '{gsub("out_user_",""); print}'
echo $LIST

